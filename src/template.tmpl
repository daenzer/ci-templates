# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0:
#
# {{ warning }}

# This template will create a {{distribution}} image based on the following variables:
#
{% if not version %}
#  - FDO_DISTRIBUTION_VERSION:  the {{distribution}} version ({{version_examples|join(', ')}}, etc...)
{% endif %}
#  - FDO_DISTRIBUTION_PACKAGES: if set, list of packages that needs to be installed
#  - FDO_DISTRIBUTION_EXEC:     if set, this command will be run once the packages have
#                               been installed
#  - FDO_DISTRIBUTION_NAME:     set by these templates to "{{distribution}}".
#                               Consider this as read-only variable.
#  - FDO_UPSTREAM_REPO:         the upstream project on this gitlab instance
#                               where we might find the given tag (for example:
#                               `wayland/weston`)
#  - FDO_REPO_SUFFIX:           The repository name suffix.
#                               If this variable isn't defined,
#                               "{{distribution}}/$FDO_DISTRIBUTION_VERSION" is used for
#                               the suffix.
#  - FDO_DISTRIBUTION_TAG:      tag to copy the image from the upstream registry. If the
#                               tag does not exist, create a new build and tag it
#  - FDO_CACHE_DIR:             A directory that needs to be mounted as `/cache` during
#                               $FDO_DISTRIBUTION_EXEC stage. Defaults to `/cache` from the
#                               host, so that most users should not need this.
#  - FDO_FORCE_REBUILD:         when set, the container will be rebuild unconditionally.
#  - FDO_EXPIRES_AFTER:         Allows to set an expiration time for the build image. Useful when
#                               images are not meant to be kept for eternity.
#                               The values are something like 1h, 2d, 3w for hour, day and weeks,
#                               respectively
#
# The resulting image will be pushed in the local registry, under:
#     $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG
#
# To build a new container, use:
#   - `.fdo.container-build@{{distribution}}`: this will rebuild a new container
#     only if $FDO_DISTRIBUTION_TAG is not available in the local registry or
#     in the $FDO_UPSTREAM_REPO registry. To force rebuilding the container,
#     set the variable $FDO_FORCE_REBUILD.

# we cannot reuse exported variables in after_script,
# so let's have a common definition
.fdo.{{distribution}}_vars: &fdo_distro_vars
    # exporting templates variables
    # https://gitlab.com/gitlab-com/support-forum/issues/4349
  - export BUILDAH_FORMAT=docker

    {# intentionally hard-coded because FDO_DISTRIBUTION_NAME
       is a read-only variable #}
  - export DISTRO={{distribution}}

  - export DISTRO_TAG=$FDO_DISTRIBUTION_TAG
{% if version %}
  - export FDO_DISTRIBUTION_VERSION="{{version}}"
{% endif %}
  - export DISTRO_VERSION="$FDO_DISTRIBUTION_VERSION"
  - export DISTRO_EXEC=$FDO_DISTRIBUTION_EXEC

  - if [ x"$FDO_REPO_SUFFIX" == x"" ] ;
    then
            export FDO_REPO_SUFFIX={{distribution}}/$DISTRO_VERSION ;
    fi

  - export BUILDAH_RUN="buildah run --isolation chroot"
  - export BUILDAH_COMMIT="buildah commit --format docker"
  - export CACHE_DIR=${FDO_CACHE_DIR:-/cache}

    # to be able to test the following script in the CI of the ci-templates
    # project itself, we need to put a special case here to have a
    # different image to pull if it already exists
  - export REPO_SUFFIX_LOCAL=$FDO_REPO_SUFFIX
  - if [[ x"$REPO_SUFFIX_LOCAL" == x"{{distribution}}/ci_templates_test_upstream" ]] ;
    then
            export FDO_REPO_SUFFIX=${DISTRO}/${DISTRO_VERSION} ;
    fi

# have a special definition for the test if the image already exists
.fdo.{{distribution}}_exists: &fdo_distro_exists |
  # if-not-exists steps
  set -x

  if [[ -z "$FDO_FORCE_REBUILD" ]]
  then
    # disable exit on failure
    set +e

    # check if our image is already in the current registry
    skopeo inspect docker://$CI_REGISTRY_IMAGE/$REPO_SUFFIX_LOCAL:$DISTRO_TAG > /dev/null && exit 0

    # check if our image is already in the upstream registry
    if [[ -z "$FDO_UPSTREAM_REPO" ]]
    then
      echo "WARNING! Variable \$FDO_UPSTREAM_REPO is undefined, cannot check for images"
    else
      skopeo inspect docker://$CI_REGISTRY/$FDO_UPSTREAM_REPO/$FDO_REPO_SUFFIX:$DISTRO_TAG > /dev/null && touch .upstream
    fi

    # reenable exit on failure
    set +e

    # copy the original image into the current project registry namespace
    # we do 2 attempts with skopeo copy at most
    if [ -f .upstream ]
    then
      skopeo copy --dest-creds $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                  docker://$CI_REGISTRY/$FDO_UPSTREAM_REPO/$FDO_REPO_SUFFIX:$DISTRO_TAG \
                  docker://$CI_REGISTRY_IMAGE/$REPO_SUFFIX_LOCAL:$DISTRO_TAG || \
      skopeo copy --dest-creds $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                  docker://$CI_REGISTRY/$FDO_UPSTREAM_REPO/$FDO_REPO_SUFFIX:$DISTRO_TAG \
                  docker://$CI_REGISTRY_IMAGE/$REPO_SUFFIX_LOCAL:$DISTRO_TAG

      exit 0
    fi
  fi

.fdo.{{distribution}}:
  variables:
    FDO_DISTRIBUTION_NAME: "{{distribution}}"

###
# Checks for a pre-existing {{distribution}} container image and builds it if
# it does not yet exist.
#
# If an image with the same version or suffix exists in the upstream project's registry,
# the image is copied into this project's registry.
# If no such image exists, the image is built and pushed to the local registry.
#
# **Reserved by this template:**
#
# - ``image:`` do not override
# - ``script:`` do not override
#
# **Variables required:**
#
# - ``FDO_UPSTREAM_REPO``: the GitLab project path to the upstream project
{% if not version %}
# - ``FDO_DISTRIBUTION_VERSION``: the {{distribution}} version to build, e.g. {{version_examples|join(', ')}}
{% endif %}
# - ``FDO_DISTRIBUTION_TAG``: string to identify the image in the registry
# - ``FDO_REPO_SUFFIX`` **(optional)**: the repository name suffix to use, see below.
#
# The resulting image will be pushed to the local registry.
#
# If the repository suffix was specified, the image path is
# ``$CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG``. The
# ``.fdo.suffixed_image@{{distribution}}`` template provides access to this image.
#
# If the repository suffix was **not** specified, the image path is
{% if version %}
# ``$CI_REGISTRY_IMAGE/{{distribution}}/{{version}}:$FDO_DISTRIBUTION_TAG``.
{% else %}
# ``$CI_REGISTRY_IMAGE/{{distribution}}/$FDO_DISTRIBUTION_VERSION:$FDO_DISTRIBUTION_TAG``.
{% endif %}
# The ``.fdo.distribution-image@{{distribution}}`` template provides access to this image.
#
.fdo.container-build@{{distribution}}:
  extends: .fdo.{{distribution}}
  image: $CI_REGISTRY/freedesktop/ci-templates/buildah:{{ bootstrap_tag }}
  stage: build
  script:
    # log in to the registry
  - podman login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

  - *fdo_distro_vars
  - if [[ x"$DISTRO_TAG" == x"" ]] ;
    then
      echo $DISTRO tag missing;
      exit 1;
    fi

  - *fdo_distro_exists

  - echo Building $FDO_REPO_SUFFIX:$DISTRO_TAG from {{ image }}
    # initial set up: take the base image, update it and install the packages
  - buildcntr=$(buildah from {{ image }})
  - buildmnt=$(buildah mount $buildcntr)

    {% if prepare %}
      {% for command in prepare %}
  - {{command.replace(" /", " $buildmnt/")}}
      {% endfor %}

    {% endif %}
    {% for command in upgrade %}
  - $BUILDAH_RUN $buildcntr {{command}}
    {% endfor %}

  - if [[ x"$FDO_DISTRIBUTION_PACKAGES" != x"" ]] ;
    then
      {% for command in install %}
      $BUILDAH_RUN $buildcntr {{command}} $FDO_DISTRIBUTION_PACKAGES ;
      {% endfor %}
    fi

    # check if there is an optional post install script and run it
  - if [[ x"$DISTRO_EXEC" != x"" ]] ;
    then
      echo Running $DISTRO_EXEC ;
      set -x ;
      mkdir $buildmnt/tmp/clone ;
      pushd $buildmnt/tmp/clone ;
      git init ;
      git remote add origin $CI_REPOSITORY_URL ;
      git fetch --depth 1 origin $CI_COMMIT_SHA ;
      git checkout FETCH_HEAD  > /dev/null;
      buildah config --workingdir /tmp/clone --env HOME="$HOME" $buildcntr ;
      if [ -e $CACHE_DIR ] ;
      then
        CACHE="-v $CACHE_DIR:/cache:rw,shared,z" ;
      fi ;
      $BUILDAH_RUN $CACHE $buildcntr bash -c "set -x ; $DISTRO_EXEC" ;
      popd ;
      rm -rf $buildmnt/tmp/clone ;
      set +x ;
    fi

    # do not store the packages database, it's pointless
    {% for command in clean %}
  - $BUILDAH_RUN $buildcntr {{command}}
    {% endfor %}

    # set up the working directory
  - mkdir -p $buildmnt/app
  - buildah config --workingdir /app $buildcntr
    # umount the container, not required, but, heh
  - buildah unmount $buildcntr
  - if [[ x"$FDO_EXPIRES_AFTER" != x"" ]] ;
    then
      buildah config -l fdo.expires-after=$FDO_EXPIRES_AFTER $buildcntr;
    fi

  - if [[ x"$FDO_UPSTREAM_REPO" != x"" ]] ;
    then
      buildah config -l fdo.upstream-repo=$FDO_UPSTREAM_REPO $buildcntr;
    fi

    # tag the current container
  - $BUILDAH_COMMIT $buildcntr $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$DISTRO_TAG
  - export JOB_TAG="${DISTRO_TAG}-built-by-job-${CI_JOB_ID}"
  - $BUILDAH_COMMIT $buildcntr $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$JOB_TAG

    # clean up the working container
  - buildah rm $buildcntr

    # push the container image to the registry
    # There is a bug when pushing 2 tags in the same repo with the same base:
    # this may fail. Just retry it after.
  - podman push $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$JOB_TAG || true
  - sleep 2
  - podman push $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$JOB_TAG

    # Push the final tag
  - podman push $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$DISTRO_TAG || true
  - sleep 2
  - podman push $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$DISTRO_TAG
{% if aarch64 %}

###
# Checks for a pre-existing {{distribution}} container image for the
# ``arm64v8`` processor architecture and builds it if it does not yet exist.
#
# This template requires runners with the ``aarch64`` tag.
#
# See ``.fdo.container-build@{{distribution}}`` for details.
.fdo.container-build@{{distribution}}@arm64v8:
  extends: .fdo.container-build@{{distribution}}
  image: $CI_REGISTRY/freedesktop/ci-templates/arm64v8/buildah:{{ bootstrap_tag }}
  tags:
    - aarch64
{% endif %}

{% if qemu %}

###
# Checks for a pre-existing {{distribution}} container image usable on
# KVM-enabled runners and builds it if it does not yet exist.
#
# This template requires runners with the ``kvm`` tag.
#
# See ``.fdo.container-build@{{distribution}}`` for details.
.fdo.qemu-build@{{distribution}}:
  extends: .fdo.container-build@{{distribution}}
  tags:
    - kvm
  image: $CI_REGISTRY/freedesktop/ci-templates/qemu-mkosi-base:{{ qemu_tag }}
  script:
  # log in to the registry
  - podman login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

  - *fdo_distro_vars

  - *fdo_distro_exists

  # start our current base mkosi image
  - /bin/bash /app/start_vm.sh -cdrom /app/my-seed.iso

  - QEMU_VERSION=$FDO_DISTRIBUTION_VERSION
{% if ubuntu_versions %}

  # for ubuntu, we need to use the name, not the number
  - |
    declare -A versions
    {% for key, value in ubuntu_versions.items() %}
    versions["{{key}}"]={{value.lower()}}
    {% endfor %}
  - if [[ x"${versions[$FDO_DISTRIBUTION_VERSION]}" != x"" ]] ;
    then
      QEMU_VERSION=${versions["$FDO_DISTRIBUTION_VERSION"]} ;
    fi
{% endif %}

  - |
    cat <<EOF > mkosi.default
    [Distribution]
    Distribution={{distribution}}
    Release=$QEMU_VERSION

    [Output]
    Format=gpt_ext4
    Bootable=yes
    BootProtocols=bios
    Password=root
    KernelCommandLine=!* selinux=0 audit=0 rw console=tty0 console=ttyS0

    [Partitions]
    RootSize=2G

    [Packages]
    # The packages to appear in both the build and the final image
    Packages=
{% for package in qemu_packages %}
      {{ package }}
{% endfor %}
    EOF

  - echo $FDO_DISTRIBUTION_PACKAGES | tr ' ' '\n' | sed -e 's/^/  /' >> mkosi.default

  # create a new ssh key
  - ssh-keygen -t rsa -f /root/.ssh/id_rsa_target -N ''
  - mkdir -p mkosi.extra/root/.ssh
  - chmod 700 mkosi.extra/root/.ssh
  - cp /root/.ssh/id_rsa_target.pub mkosi.extra/root/.ssh/authorized_keys
  - chmod 600 mkosi.extra/root/.ssh/authorized_keys
{% if ubuntu_versions %}

  # ubuntu disables NetworkManager through /usr/lib/NetworkManager/conf.d/10-globally-managed-devices.conf
  # overwrite it in /etc to allow NM to manage ens3
  - mkdir -p mkosi.extra/etc/NetworkManager/conf.d/
  - touch mkosi.extra/etc/NetworkManager/conf.d/10-globally-managed-devices.conf
{% endif %}

  # enable sshd on the target
  - mkdir -p mkosi.extra/etc/systemd/system/multi-user.target.wants

  # send the mkosi files to the VM
  - scp -P 5555 mkosi.default localhost:/root/mkosi.default
  - scp -P 5555 -r mkosi.extra localhost:/root/

  # enable sshd on the target
  - ssh localhost -p 5555 ln -s /usr/lib/systemd/system/sshd.service
                                mkosi.extra/etc/systemd/system/multi-user.target.wants/sshd.service

  # create a cache folder (useful only when manually testing this script)
  - ssh localhost -p 5555 mkdir mkosi.cache

  # run mkosi in the VM!
  - ssh localhost -p 5555 mkosi/mkosi

  # mount the root partition locally to extract the kernel and initramfs
  - ssh localhost -p 5555 mkdir loop
  - offset=$(ssh localhost -p 5555 fdisk -l image.raw | grep image.raw2 | cut -d ' ' -f 3)
  - ssh localhost -p 5555 mount -o ro,loop,offset=$(($offset * 512)) image.raw loop/

  # fetch kernel and initramfs
  - ssh localhost -p 5555 ls loop/boot/
  - ssh localhost -p 5555 "cp loop/boot/vmlinuz* loop/boot/initr* ."

  - ssh localhost -p 5555 umount loop/

  # now compress the image (we wanted to extract first the kernel and initrd)
  - ssh localhost -p 5555 xz -T0 image.raw

  # fetch the image and kernel
  - scp -P 5555 localhost:image.raw.xz dest-image.raw.xz

  - scp -P 5555 localhost:vmlinuz\* localhost:initr\* .

  # terminate qemu
  - kill $(pgrep qemu) || true

  # building the final image
  - |
    cat > /etc/containers/storage.conf <<EOF
    [storage]
    driver = "vfs"
    runroot = "/var/run/containers/storage"
    graphroot = "/var/lib/containers/storage"
    EOF

  - QEMU_BASE_IMAGE=${QEMU_BASE_IMAGE:-$CI_REGISTRY/freedesktop/ci-templates/qemu-base:{{ qemu_tag }}}

  - echo Building $FDO_REPO_SUFFIX:$DISTRO_TAG from $QEMU_BASE_IMAGE

  - buildcntr=$(buildah from $QEMU_BASE_IMAGE)
  - buildmnt=$(buildah mount $buildcntr)

  # insert our final VM image we just built
  - mkdir -p $buildmnt/app
  - mv dest-image.raw.xz $buildmnt/app/image.raw.xz
  - mv vmlinuz* initr* $buildmnt/app/
  - mkdir $buildmnt/root/.ssh
  - chmod 700 $buildmnt/root/.ssh
  - cp /root/.ssh/id_rsa_target $buildmnt/root/.ssh/id_rsa
  - cp /root/.ssh/id_rsa_target.pub $buildmnt/root/.ssh/id_rsa.pub

  - |
    cat > $buildmnt/app/start_vm_kernel.sh <<EOF
    #!/bin/bash

    set -x

    KERNEL=\$1
    shift

    set -e

    if [[ x"\$KERNEL" == x"" ]]
    then
      KERNEL=\$(ls /app/vmlinuz* | sort | tail -1)
    fi

    INITRD=\$(ls /app/initr* | sort | tail -1)

    bash /app/start_vm.sh -kernel \$KERNEL \\
                          -initrd \$INITRD \\
                          -append "root=/dev/sda2 selinux=0 audit=0 rw console=tty0 console=ttyS0" \\
                          "\$@"

    EOF
  - chmod +x $buildmnt/app/start_vm_kernel.sh

    # umount the container, not required, but, heh
  - buildah unmount $buildcntr

  - if [[ x"$FDO_EXPIRES_AFTER" != x"" ]] ;
    then
      buildah config -l fdo.expires-after=$FDO_EXPIRES_AFTER $buildcntr;
    fi

  - if [[ x"$FDO_UPSTREAM_REPO" != x"" ]] ;
    then
      buildah config -l fdo.upstream-repo=$FDO_UPSTREAM_REPO $buildcntr;
    fi

    # tag the current container
  - $BUILDAH_COMMIT $buildcntr $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$DISTRO_TAG

    # clean up the working container
  - buildah rm $buildcntr

    # push the container image to the registry
    # There is a bug when pushing 2 tags in the same repo with the same base:
    # this may fail. Just retry it after.
  - podman push $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$DISTRO_TAG || true
  - sleep 2
  - podman push $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$DISTRO_TAG
{% endif %}

###
# {{distribution}} template that pulls the {{distribution}} image from the
# registry based on {% if not version %}``FDO_DISTRIBUTION_VERSION`` and {% endif -%} ``FDO_DISTRIBUTION_TAG``.
# This template must be provided the same variable values as supplied in
# ``container-build``.
#
# This template sets ``image:`` to the generated image. You may override this.
#
# **Variables required:**
#
{% if not version %}
# - ``FDO_DISTRIBUTION_VERSION``: the {{distribution}} version to fetch, e.g. {{version_examples|join(', ')}}
{% endif %}
# - ``FDO_DISTRIBUTION_TAG``: string to identify the image in the registry
#
# **Variables provided by this template:**
#
# - ``FDO_DISTRIBUTION_IMAGE``: path to the registry image
# - ``FDO_DISTRIBUTION_NAME``: set to "{{distribution}}"
#
# Variables provided by this template should be considered read-only.
#
# .. note:: If you used ``FDO_REPO_SUFFIX`` when building the container, use
#           ``.fdo.suffixed_image@{{distribution}}`` instead.
.fdo.distribution-image@{{distribution}}:
  extends: .fdo.{{distribution}}
{% if version %}
  image: $CI_REGISTRY_IMAGE/{{distribution}}/{{version}}:$FDO_DISTRIBUTION_TAG
{% else %}
  image: $CI_REGISTRY_IMAGE/{{distribution}}/$FDO_DISTRIBUTION_VERSION:$FDO_DISTRIBUTION_TAG
{% endif %}
  variables:
{% if version %}
    FDO_DISTRIBUTION_IMAGE: $CI_REGISTRY_IMAGE/{{distribution}}/{{version}}:$FDO_DISTRIBUTION_TAG
{% else %}
    FDO_DISTRIBUTION_IMAGE: $CI_REGISTRY_IMAGE/{{distribution}}/$FDO_DISTRIBUTION_VERSION:$FDO_DISTRIBUTION_TAG
{% endif %}

###
# {{distribution}} template that pulls the {{distribution}} image from the
# registry based on ``FDO_REPO_SUFFIX``.
# This template must be provided the same variable values as supplied in
# ``container-build``.
#
# This template sets ``image:`` to the generated image. You may override this.
#
# **Variables required:**
#
# - ``FDO_REPO_SUFFIX``: the repository name suffix
# - ``FDO_DISTRIBUTION_TAG``: string to identify the image in the registry
#
# **Variables provided by this template:**
#
# - ``FDO_DISTRIBUTION_IMAGE``: path to the registry image
# - ``FDO_DISTRIBUTION_NAME``: set to "{{distribution}}"
#
#
# Variables provided by this template should be considered read-only.
#
# .. note:: If you did not use ``FDO_REPO_SUFFIX`` when building the container, use
#           ``.fdo.distribution-image@{{distribution}}`` instead.
.fdo.suffixed_image@{{distribution}}:
  extends: .fdo.{{distribution}}
  image: $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG
  variables:
    FDO_DISTRIBUTION_IMAGE: $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG
