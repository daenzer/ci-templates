# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0:
#
# THIS FILE IS GENERATED, DO NOT EDIT

# This template will create a centos image based on the following variables:
#
#  - FDO_DISTRIBUTION_VERSION:  the centos version (7, 8, etc...)
#  - FDO_DISTRIBUTION_PACKAGES: if set, list of packages that needs to be installed
#  - FDO_DISTRIBUTION_EXEC:     if set, this command will be run once the packages have
#                               been installed
#  - FDO_DISTRIBUTION_NAME:     set by these templates to "centos".
#                               Consider this as read-only variable.
#  - FDO_UPSTREAM_REPO:         the upstream project on this gitlab instance
#                               where we might find the given tag (for example:
#                               `wayland/weston`)
#  - FDO_REPO_SUFFIX:           The repository name suffix.
#                               If this variable isn't defined,
#                               "centos/$FDO_DISTRIBUTION_VERSION" is used for
#                               the suffix.
#  - FDO_DISTRIBUTION_TAG:      tag to copy the image from the upstream registry. If the
#                               tag does not exist, create a new build and tag it
#  - FDO_CACHE_DIR:             A directory that needs to be mounted as `/cache` during
#                               $FDO_DISTRIBUTION_EXEC stage. Defaults to `/cache` from the
#                               host, so that most users should not need this.
#  - FDO_FORCE_REBUILD:         when set, the container will be rebuild unconditionally.
#  - FDO_EXPIRES_AFTER:         Allows to set an expiration time for the build image. Useful when
#                               images are not meant to be kept for eternity.
#                               The values are something like 1h, 2d, 3w for hour, day and weeks,
#                               respectively
#
# The resulting image will be pushed in the local registry, under:
#     $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG
#
# To build a new container, use:
#   - `.fdo.container-build@centos`: this will rebuild a new container
#     only if $FDO_DISTRIBUTION_TAG is not available in the local registry or
#     in the $FDO_UPSTREAM_REPO registry. To force rebuilding the container,
#     set the variable $FDO_FORCE_REBUILD.

# we cannot reuse exported variables in after_script,
# so let's have a common definition
.fdo.centos_vars: &fdo_distro_vars
    # exporting templates variables
    # https://gitlab.com/gitlab-com/support-forum/issues/4349
  - export BUILDAH_FORMAT=docker

  - export DISTRO=centos

  - export DISTRO_TAG=$FDO_DISTRIBUTION_TAG
  - export DISTRO_VERSION="$FDO_DISTRIBUTION_VERSION"
  - export DISTRO_EXEC=$FDO_DISTRIBUTION_EXEC

  - if [ x"$FDO_REPO_SUFFIX" == x"" ] ;
    then
            export FDO_REPO_SUFFIX=centos/$DISTRO_VERSION ;
    fi

  - export BUILDAH_RUN="buildah run --isolation chroot"
  - export BUILDAH_COMMIT="buildah commit --format docker"
  - export CACHE_DIR=${FDO_CACHE_DIR:-/cache}

    # to be able to test the following script in the CI of the ci-templates
    # project itself, we need to put a special case here to have a
    # different image to pull if it already exists
  - export REPO_SUFFIX_LOCAL=$FDO_REPO_SUFFIX
  - if [[ x"$REPO_SUFFIX_LOCAL" == x"centos/ci_templates_test_upstream" ]] ;
    then
            export FDO_REPO_SUFFIX=${DISTRO}/${DISTRO_VERSION} ;
    fi

# have a special definition for the test if the image already exists
.fdo.centos_exists: &fdo_distro_exists |
  # if-not-exists steps
  set -x

  if [[ -z "$FDO_FORCE_REBUILD" ]]
  then
    # disable exit on failure
    set +e

    # check if our image is already in the current registry
    skopeo inspect docker://$CI_REGISTRY_IMAGE/$REPO_SUFFIX_LOCAL:$DISTRO_TAG > /dev/null && exit 0

    # check if our image is already in the upstream registry
    if [[ -z "$FDO_UPSTREAM_REPO" ]]
    then
      echo "WARNING! Variable \$FDO_UPSTREAM_REPO is undefined, cannot check for images"
    else
      skopeo inspect docker://$CI_REGISTRY/$FDO_UPSTREAM_REPO/$FDO_REPO_SUFFIX:$DISTRO_TAG > /dev/null && touch .upstream
    fi

    # reenable exit on failure
    set +e

    # copy the original image into the current project registry namespace
    # we do 2 attempts with skopeo copy at most
    if [ -f .upstream ]
    then
      skopeo copy --dest-creds $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                  docker://$CI_REGISTRY/$FDO_UPSTREAM_REPO/$FDO_REPO_SUFFIX:$DISTRO_TAG \
                  docker://$CI_REGISTRY_IMAGE/$REPO_SUFFIX_LOCAL:$DISTRO_TAG || \
      skopeo copy --dest-creds $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                  docker://$CI_REGISTRY/$FDO_UPSTREAM_REPO/$FDO_REPO_SUFFIX:$DISTRO_TAG \
                  docker://$CI_REGISTRY_IMAGE/$REPO_SUFFIX_LOCAL:$DISTRO_TAG

      exit 0
    fi
  fi

.fdo.centos:
  variables:
    FDO_DISTRIBUTION_NAME: "centos"

###
# Checks for a pre-existing centos container image and builds it if
# it does not yet exist.
#
# If an image with the same version or suffix exists in the upstream project's registry,
# the image is copied into this project's registry.
# If no such image exists, the image is built and pushed to the local registry.
#
# **Reserved by this template:**
#
# - ``image:`` do not override
# - ``script:`` do not override
#
# **Variables required:**
#
# - ``FDO_UPSTREAM_REPO``: the GitLab project path to the upstream project
# - ``FDO_DISTRIBUTION_VERSION``: the centos version to build, e.g. 7, 8
# - ``FDO_DISTRIBUTION_TAG``: string to identify the image in the registry
# - ``FDO_REPO_SUFFIX`` **(optional)**: the repository name suffix to use, see below.
#
# The resulting image will be pushed to the local registry.
#
# If the repository suffix was specified, the image path is
# ``$CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG``. The
# ``.fdo.suffixed_image@centos`` template provides access to this image.
#
# If the repository suffix was **not** specified, the image path is
# ``$CI_REGISTRY_IMAGE/centos/$FDO_DISTRIBUTION_VERSION:$FDO_DISTRIBUTION_TAG``.
# The ``.fdo.distribution-image@centos`` template provides access to this image.
#
.fdo.container-build@centos:
  extends: .fdo.centos
  image: $CI_REGISTRY/freedesktop/ci-templates/buildah:2020-03-11
  stage: build
  script:
    # log in to the registry
  - podman login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

  - *fdo_distro_vars
  - if [[ x"$DISTRO_TAG" == x"" ]] ;
    then
      echo $DISTRO tag missing;
      exit 1;
    fi

  - *fdo_distro_exists

  - echo Building $FDO_REPO_SUFFIX:$DISTRO_TAG from $DISTRO:$DISTRO_VERSION
    # initial set up: take the base image, update it and install the packages
  - buildcntr=$(buildah from $DISTRO:$DISTRO_VERSION)
  - buildmnt=$(buildah mount $buildcntr)

  - $BUILDAH_RUN $buildcntr dnf --help >/dev/null 2>&1 && DNF=dnf || DNF=yum
  - $BUILDAH_RUN $buildcntr $DNF upgrade -y

  - if [[ x"$FDO_DISTRIBUTION_PACKAGES" != x"" ]] ;
    then
      $BUILDAH_RUN $buildcntr $DNF install -y $FDO_DISTRIBUTION_PACKAGES ;
    fi

    # check if there is an optional post install script and run it
  - if [[ x"$DISTRO_EXEC" != x"" ]] ;
    then
      echo Running $DISTRO_EXEC ;
      set -x ;
      mkdir $buildmnt/tmp/clone ;
      pushd $buildmnt/tmp/clone ;
      git init ;
      git remote add origin $CI_REPOSITORY_URL ;
      git fetch --depth 1 origin $CI_COMMIT_SHA ;
      git checkout FETCH_HEAD  > /dev/null;
      buildah config --workingdir /tmp/clone --env HOME="$HOME" $buildcntr ;
      if [ -e $CACHE_DIR ] ;
      then
        CACHE="-v $CACHE_DIR:/cache:rw,shared,z" ;
      fi ;
      $BUILDAH_RUN $CACHE $buildcntr bash -c "set -x ; $DISTRO_EXEC" ;
      popd ;
      rm -rf $buildmnt/tmp/clone ;
      set +x ;
    fi

    # do not store the packages database, it's pointless
  - $BUILDAH_RUN $buildcntr $DNF clean all

    # set up the working directory
  - mkdir -p $buildmnt/app
  - buildah config --workingdir /app $buildcntr
    # umount the container, not required, but, heh
  - buildah unmount $buildcntr
  - if [[ x"$FDO_EXPIRES_AFTER" != x"" ]] ;
    then
      buildah config -l fdo.expires-after=$FDO_EXPIRES_AFTER $buildcntr;
    fi

  - if [[ x"$FDO_UPSTREAM_REPO" != x"" ]] ;
    then
      buildah config -l fdo.upstream-repo=$FDO_UPSTREAM_REPO $buildcntr;
    fi

    # tag the current container
  - $BUILDAH_COMMIT $buildcntr $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$DISTRO_TAG
  - export JOB_TAG="${DISTRO_TAG}-built-by-job-${CI_JOB_ID}"
  - $BUILDAH_COMMIT $buildcntr $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$JOB_TAG

    # clean up the working container
  - buildah rm $buildcntr

    # push the container image to the registry
    # There is a bug when pushing 2 tags in the same repo with the same base:
    # this may fail. Just retry it after.
  - podman push $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$JOB_TAG || true
  - sleep 2
  - podman push $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$JOB_TAG

    # Push the final tag
  - podman push $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$DISTRO_TAG || true
  - sleep 2
  - podman push $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$DISTRO_TAG


###
# centos template that pulls the centos image from the
# registry based on ``FDO_DISTRIBUTION_VERSION`` and ``FDO_DISTRIBUTION_TAG``.
# This template must be provided the same variable values as supplied in
# ``container-build``.
#
# This template sets ``image:`` to the generated image. You may override this.
#
# **Variables required:**
#
# - ``FDO_DISTRIBUTION_VERSION``: the centos version to fetch, e.g. 7, 8
# - ``FDO_DISTRIBUTION_TAG``: string to identify the image in the registry
#
# **Variables provided by this template:**
#
# - ``FDO_DISTRIBUTION_IMAGE``: path to the registry image
# - ``FDO_DISTRIBUTION_NAME``: set to "centos"
#
# Variables provided by this template should be considered read-only.
#
# .. note:: If you used ``FDO_REPO_SUFFIX`` when building the container, use
#           ``.fdo.suffixed_image@centos`` instead.
.fdo.distribution-image@centos:
  extends: .fdo.centos
  image: $CI_REGISTRY_IMAGE/centos/$FDO_DISTRIBUTION_VERSION:$FDO_DISTRIBUTION_TAG
  variables:
    FDO_DISTRIBUTION_IMAGE: $CI_REGISTRY_IMAGE/centos/$FDO_DISTRIBUTION_VERSION:$FDO_DISTRIBUTION_TAG

###
# centos template that pulls the centos image from the
# registry based on ``FDO_REPO_SUFFIX``.
# This template must be provided the same variable values as supplied in
# ``container-build``.
#
# This template sets ``image:`` to the generated image. You may override this.
#
# **Variables required:**
#
# - ``FDO_REPO_SUFFIX``: the repository name suffix
# - ``FDO_DISTRIBUTION_TAG``: string to identify the image in the registry
#
# **Variables provided by this template:**
#
# - ``FDO_DISTRIBUTION_IMAGE``: path to the registry image
# - ``FDO_DISTRIBUTION_NAME``: set to "centos"
#
#
# Variables provided by this template should be considered read-only.
#
# .. note:: If you did not use ``FDO_REPO_SUFFIX`` when building the container, use
#           ``.fdo.distribution-image@centos`` instead.
.fdo.suffixed_image@centos:
  extends: .fdo.centos
  image: $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG
  variables:
    FDO_DISTRIBUTION_IMAGE: $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG